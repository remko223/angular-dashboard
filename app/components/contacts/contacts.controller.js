/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('ContactController', ContactCtrl);

    ContactCtrl.$inject = ['session', '$scope', 'InfiniteScroll', 'dataService', 'SpinnerService', 'growl', '_'];

    function ContactCtrl(session, $scope, InfiniteScroll, dataService, SpinnerService, growl, _) {
        var vm = this,
            templates = {
                none: 'app/components/contacts/contact-none.html',
                details: 'app/components/contacts/contact-details.html'
            },
            contactBlank = {
                lastName: '',
                firstName: '',
                company: '',
                birthDate: '',
                homePhone: '',
                workPhone: '',
                mobilePhone: '',
                otherPhone: '',
                fax: '',
                homeEmail: '',
                workEmail: '',
                otherEmail: '',
                webPage: '',
                note: '',
                HomeAddress: {
                    street1: '',
                    street2: '',
                    city: '',
                    state: '',
                    zip: '',
                    country: ''
                },
                WorkAddress: {
                    street1: '',
                    street2: '',
                    city: '',
                    state: '',
                    zip: '',
                    country: ''
                }
            };

        vm.pager = new InfiniteScroll('contacts', {device_id: session.deviceId});
        vm.selectContact = selectContact;
        vm.closeContact = closeContact;
        vm.template = templates.none;
        vm.contactFilled = contactFilled;
        vm.addContact = addContact;
        vm.saveContact = saveContact;

        activate();

        function activate() {
            vm.container = angular.element('#record-container');
            vm.detailsTemplate = 'app/components/contacts/contact-details-view.html';

            $scope.$watch('ctrl.pager.device.is_android', function (newValue) {
                vm.detailsTemplate = 'app/components/contacts/' +
                    (newValue ? 'contactDetails.html' : 'contact-details-view.html');
            });

            $scope.$on('removePagerItem', function (event, rowData) {
                var idx = $scope.pager.results.indexOf(rowData);

                $scope.pager.results.splice(idx, 1);
            });
        }

        function selectContact(contact) {
            SpinnerService.show();

            dataService
                .contacts(0, 1, $scope.deviceId, contact.device_contact.id)
                .then(function (response) {
                    vm.template = templates.details;
                    vm.contact = response.content;

                    if (angular.isString(vm.contact.contact_json)) {
                        vm.contact.contact_json = angular.fromJson(vm.contact.contact_json);
                    }

                    if (!vm.contact.contact_json) {
                        vm.contact.contact_json = {
                            firstName: vm.contact.first_name,
                            lastName: vm.contact.last_name
                        };
                    } else {
                        vm.contact.contact_json.firstName = vm.contact.contact_json.firstName || vm.contact.first_name;
                        vm.contact.contact_json.lastName = vm.contact.contact_json.lastName || vm.contact.last_name;
                    }
                })
                .finally(function () {
                    SpinnerService.hide();
                });
        }

        function closeContact() {
            vm.contact = undefined;
            vm.template = templates.none;
        }

        function contactFilled(value, index, array) {
            var contact = value.log_device_contact;

            return contact.first_name || contact.contact_name;
        }

        function addContact() {
            var tempContact = {};

            tempContact.contact_json = angular.copy(contactBlank);
            tempContact.activity = [];

            vm.contact = tempContact;
            vm.template = templates.details;
        }

        function saveContact() {
            var contact = vm.contact,
                cj = _.compactObject(contact.contact_json);

            if (contact.id) {
                // Save
                dataService
                    .contact
                    .update(
                        contact.id,
                        cj.firstName,
                        cj.lastName,
                        cj)
                    .then(function (response) {
                        growl.success('Contact successfully saved', {title: 'Contact', ttl: 10000});
                    })
                    .catch(function (err) {
                        growl.error('Unable to update contact', {title: 'Contact', ttl: 15000});
                    });
            } else {
                // Add
                cj.HomeAddress = _.compactObject(cj.HomeAddress);
                cj.WorkAddress = _.compactObject(cj.WorkAddress);

                dataService
                    .contact
                    .create(
                        session.deviceId,
                        cj.firstName,
                        cj.lastName,
                        cj)
                    .then(function (response) {
                        growl.success('Contact successfully added', {title: 'Contact', ttl: 10000});
                    })
                    .catch(function (err) {
                        growl.error('Unable to add contact', {title: 'Contact', ttl: 15000});
                    });
            }
        }
    }
}());
