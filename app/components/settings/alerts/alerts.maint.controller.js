﻿/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('AlertsMaintController', AlertsCtrl);

    AlertsCtrl.$inject = ['alertAddressData', '$scope', 'SpinnerService'];

    function AlertsCtrl(alertAddressData, $scope, SpinnerService) {
        var vm = this;

        vm.add = add;

        activate();

        function activate() {
            vm.newaddress = {
                is_sms: $scope.$state.is('secure.alerts.sms')
            };
        }

        function add(address) {
            var updateAddress;

            if (!$scope.alertForm.$valid) {
                return;
            }

            SpinnerService.show();

            updateAddress = {email: address.email, is_sms: address.is_sms};

            alertAddressData.create(updateAddress)
                .then(function (response) {
                    if (response) {
                        $scope.$state.go('secure.settings.alerts', {}, {reload: true});
                    }
                })
                .finally(SpinnerService.hide);
        }

        function emptyAddress() {
            return {description: '', email: '', is_sms: false};
        }
    }
}());
