﻿/* jshint camelcase:false, maxparams:12 */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('DashboardController', DashboardCtrl);

    DashboardCtrl.$inject = [
        '$scope', 'dataService', 'session', 'growl', 'SpinnerService',
        'locationService', 'Upload', '$uibModal', 'appConfig', '$log',
        'ngDialog', '$http'];

    function DashboardCtrl($scope, dataService, session, growl, SpinnerService,
                           locationService, Upload, $uibModal, appConfig, $log,
                           ngDialog, $http) {
        var vm = this, dialog = null;

        vm.showUpload = showUpload;
        vm.resendLink = resendLink;
        vm.lastLocation = lastLocation;

        activate();

        function activate() {
            $scope.$emit('deviceChange');
            $scope.$watch('secure.selectedDevice', function (newValue) {
                if (newValue) {
                    selectDevice(newValue);
                }

                try {
                    if (newValue.is_configured && angular.isDefined(newValue.is_msg_shown) && !newValue.is_msg_shown) {
                        if (angular.isDefined(session.deviceId) && session.deviceId !== null) {
                            $http({
                                method: 'POST',
                                url: appConfig.apiUrl + '/update_android_device_msg_status',
                                data: {device_id:session.deviceId},
                                headers: {'Content-Type': 'application/json'}
                            }).success(function (data) {
                                $scope.$emit('deviceChange');
                            });

                            dialog = ngDialog.open({
                                template: 'app/components/dashboard/dashboard-android-message.html',
                                scope: $scope
                            });
                        }
                    }
                } catch (e) {}
            });

            $scope.$watch('secure.devices', function (newValue) {
                if (newValue && newValue.length === 0) {
                    $scope.$state.go('secure.wizard.choose');
                } else if (newValue && newValue.length !== 0) {
                    if (!session.deviceId) {
                        selectDevice(newValue[0].id);
                    }
                }
            });

            vm.mapDefaults = {
                scrollWheelZoom: false
            };
        }

        function showUpload() {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'app/components/dashboard/upload.html',
                controller: UploadCtrl,
                controllerAs: 'uploadCtrl'
            });

            modalInstance.result.then(function (file) {
                uploadFile(file);
            }, function () {
                // Dismissed
            });

            UploadCtrl.$inject = ['$scope', '$uibModalInstance'];

            function UploadCtrl($scope, $uibModalInstance) {
                var vm = this;

                vm.submit = submit;
                vm.cancel = cancel;

                function submit() {
                    if ($scope.uploadForm.file.$valid && vm.file) {
                        $uibModalInstance.close(vm.file);
                    }
                }

                function cancel() {
                    $uibModalInstance.dismiss('canceled');
                }
            }
        }

        function uploadFile(file) {
            Upload.upload({
                url: appConfig.avatarUrl + '/upload',
                data: {pic: file, device_id: session.deviceId}
            }).then(function (resp) {
                $scope.$state.go('secure.dashboard', {}, {reload: true});

                $log.info('Success. Response: ' + resp.data);
            }, function (resp) {
                $log.info('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

                $log.info('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }

        function resendLink(deviceId) {
            SpinnerService.show();

            dataService
                .sendSmsLink(deviceId)
                .then(function (response) {
                    growl.success('Configuration Link Sent', {title: 'Send Link', ttl: 5000});
                })
                .catch(function (err) {
                    growl.error('Unable to resend the link right now, please try again later', {
                        title: 'Resend Link',
                        ttl: 10000
                    });
                })
                .finally(SpinnerService.hide);
        }

        function selectDevice(device) {
            if (!device.is_configured) {
                if (device.is_android) {
                    $scope.$state.go('^.config-android');
                } else if (session.isActiveSubscriber) {
                    $scope.$state.go('^.config-ios');
                }
            }

            vm.unread = device.unread;

            if (device.locations && device.locations.length) {
                vm.markers = [locationService.createMarker(device.locations[0], true)];

                vm.center = {
                    lat: vm.markers[0].lat,
                    lng: vm.markers[0].lng,
                    zoom: 15
                };
            }
        }

        function lastLocation() {
            SpinnerService.show();

            locationService
                .lastLocation(session.deviceId)
                .then(function (markers) {
                    vm.markers = markers;

                    vm.center = {
                        lat: vm.markers[0].lat,
                        lng: vm.markers[0].lng,
                        zoom: 15
                    };
                })
                .finally(SpinnerService.hide);
        }
    }
}());
