(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('DeepLinkController', DeepLinkController);

    DeepLinkController.$inject = ['$scope', 'DeepLinkData', 'growl'];

    /* @ngInject */
    function DeepLinkController($scope, DeepLinkData, growl) {
        var vm = this;

        vm.id = $scope.$stateParams.id;

        activate();

        function activate() {
            DeepLinkData
                .read(vm.id)
                .then(function (response) {
                    growl.success(response.message);
                })
                .catch(function (err) {
                    growl.warn('Unable to process your request');
                })
                .finally(function () {
                    $scope.$state.go('secure.dashboard');
                });
        }
    }
})();
