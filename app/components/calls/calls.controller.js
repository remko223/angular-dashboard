/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('CallController', CallCtrl);

    CallCtrl.$inject = ['session', '$scope', 'InfiniteScroll', 'dataService', '$filter', 'SpinnerService'];

    function CallCtrl(session, $scope, InfiniteScroll, dataService, $filter, SpinnerService) {
        var vm = this,
            templates = {
                none: 'app/components/calls/call-blank.html',
                details: 'app/components/calls/call-details.html'
            };

        vm.pager = new InfiniteScroll('calls', {device_id: session.deviceId});
        vm.selectCall = selectCall;
        vm.template = templates.none;
        vm.closeCall = closeCall;

        activate();

        function activate() {
            vm.container = angular.element('#record-container');

            $scope.$on('removePagerItem', function (event, rowData) {
                var idx = $scope.pager.results.indexOf(rowData);

                $scope.pager.results.splice(idx, 1);
            });
        }

        function selectCall(call) {
            SpinnerService.show();

            dataService
                .calls(0, 100, session.deviceId, call.contact_id)
                .then(function (response) {
                    var filter = $filter('orderBy'),
                        results = [];

                    vm.call = call;
                    vm.template = templates.details;

                    angular.forEach(response.calls, function (value, key) {
                        results = results.concat(value);
                    });

                    vm.callsPerContact = filter(results, 'device_created_on', true);
                })
                .finally(function () {
                    SpinnerService.hide();
                });
        }

        function closeCall() {
            vm.template = templates.none;
            vm.call = undefined;
        }
    }
}());
