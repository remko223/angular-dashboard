﻿/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('MainController', MainCtrl);

    MainCtrl.$inject = ['$window', '$scope', 'userService', 'session'];

    function MainCtrl($window, $scope, userService, session) {
        var vm = this,
            pageTitle = 'My Mobile Watchdog';

        vm.showBack = false;
        vm.loggedIn = false;
        vm.user = userService;
        vm.goBack = goBack;
        vm.logout = logout;
        vm.goDashboard = goDashboard;

        Object.defineProperty(vm, 'pageTitle', {
            get: function () {
                return pageTitle;
            },
            set: function (newValue) {
                pageTitle = newValue;

                vm.showBack = (newValue !== 'My Mobile Watchdog' && newValue !== 'Log In');
            }
        });

        activate();

        function activate() {
            $scope.$on('$viewContentLoading', function (event, viewName) {
                if (angular.isDefined(viewName.view) && viewName.view.data && viewName.view.data.pageTitle) {
                    vm.pageTitle = viewName.view.data.pageTitle;
                } else {
                    vm.pageTitle = 'My Mobile Watchdog';
                }
            });
        }

        function logout() {
            userService
                .logout()
                .then(function (response) {
                    // Need to invalidate all session variables
                    session.isActiveSubscriber = session.loggedIn = vm.loggedIn = false;
                    session.deviceId = undefined;

                    $scope.$state.go('login', null, {reload: true});
                });
        }

        function goBack() {
            $window.history.back();
        }

        function goDashboard() {
            if (!session.loggedIn) {
                $scope.$state.go('login');
            } else {
                $scope.$state.go('secure.dashboard', null, {reload: true});
            }
        }
    }
}());

