/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('InfiniteScrollListController', Ctrl);

    Ctrl.$inject = ['session', '$scope', 'InfiniteScroll', '$window'];

    function Ctrl(session, $scope, InfiniteScroll, $window) {
        var vm = this;

        vm.pager = new InfiniteScroll($scope.$state.current.data.endpoint, {device_id: session.deviceId});

        vm.container = angular.element('#record-container');

        $scope.$on('removePagerItem', function (event, rowData) {
            var idx = vm.pager.results.indexOf(rowData);
            vm.pager.results.splice(idx, 1);
        });
    }
}());
