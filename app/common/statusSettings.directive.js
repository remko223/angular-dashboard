/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('statusSettings', Directive);

    Directive.$inject = ['setStatus'];

    function Directive(setStatus) {
        function doLink(scope, element, attrs) {
            scope.is_android = attrs.android;
            scope.show = {
                menu: false,
                divider: false,
                notify: false,
                block: false,
                tempAllow: false,
                trash: false,
                allow: false,
                disable: false,
                enable: false
            };
            switch (attrs.page) {

                case 'contacts':
                    scope.show.menu = true;
                    scope.show.notify = true;
                    scope.show.allow = true;
                    if (scope.is_android) {
                        scope.show.divider = true;
                        scope.show.trash = true;
                    }
                    break;

                case 'timeblocks':
                    scope.show.menu = true;
                    scope.show.enable = true;
                    scope.show.disable = true;
                    scope.show.trash = true;
                    scope.show.divider = true;
                    break;

                case 'applications':
                    if (scope.is_android) {
                        scope.show.menu = true;
                        scope.show.allow = true;
                        scope.show.tempAllow = true;
                        scope.show.block = true;
                    }
                    break;

                case 'websites':
                    scope.show.menu = true;
                    scope.show.trash = true;
                    if (scope.is_android) {
                        scope.show.divider = true;
                        scope.show.allow = true;
                        scope.show.notify = true;
                        scope.show.block = true;
                    }
                    break;

            }
            scope.updateStatus = function (status) {
                setStatus.updateStatus(scope, attrs.page, status);
            };
        }

        return {
            restrict: 'E',
            scope: {
                page: '@',
                android: '@',
                rowData: '='
            },
            link: doLink,
            templateUrl: 'app/common/statusSettings.template.html'
        };
    }
}());

