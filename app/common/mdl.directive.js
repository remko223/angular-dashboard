(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .directive('mdl', Mdl);

    Mdl.$inject = ['componentHandler'];

    function Mdl(componentHandler) {
        function link(scope, el, attrs, ctrl, transcludeFn) {
            componentHandler.upgradeElement(el[0]);
        }

        return {
            restrict: 'A',
            link: link
        };
    }
}());
