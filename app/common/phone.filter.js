(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .filter('phone', phone);

    function phone() {
        return phoneFilter;

        function phoneFilter(text) {
            if (text && text.match(/^[0-9]{10}$/)) {
                return text.replace(/^([0-9]{3})([0-9]{3})([0-9]{4})$/, '+1 ($1) $2-$3');
            } else {
                return text;
            }
        }
    }
})();
