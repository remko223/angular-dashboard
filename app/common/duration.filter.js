/* jshint camelcase: false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .filter('wdDuration', formatDuration);

    function formatDuration() {
        /**
         * This function will turn seconds into a nicer format
         */
        return function (input) {
            var secNum = parseInt(input, 10),
                hours = Math.floor(secNum / 3600),
                minutes = Math.floor((secNum - (hours * 3600)) / 60),
                seconds = secNum - (hours * 3600) - (minutes * 60);

            if (hours > 0) {
                return hours + ' hours, ' + minutes + ' mins';
            }

            if (minutes > 0) {
                return minutes + ' mins, ' + seconds + ' secs';
            }

            return seconds + ' secs';
        };
    }
}());
