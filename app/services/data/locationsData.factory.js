/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('locationsData', LocationsDataFactory);

    LocationsDataFactory.$inject = ['dataServiceSimple'];

    function LocationsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'locations';

        dataFactory.locations = locations;
        dataFactory.location = {
            getLastKnownLocation: recentLocation,
            requestCurrentLocation: requestLocation
        };

        return dataFactory;

        function locations(offset, limit, deviceId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId
            };
            return dataService.get(endpoint, args);
        }

        function recentLocation(deviceId) {
            return locations(0, 1, deviceId);
        }

        function requestLocation(deviceId) {
            return dataService.update(endpoint, {device_id: deviceId});
        }
    }
}());
