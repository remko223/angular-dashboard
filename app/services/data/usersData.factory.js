/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('usersData', UsersDataFactory);

    UsersDataFactory.$inject = ['dataServiceSimple'];

    function UsersDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'users';

        dataFactory.users = users;
        dataFactory.user = {
            updatePassword: updatePassword
        };

        return dataFactory;

        function users() {
            return dataService.get(endpoint);
        }

        function updatePassword(userId, currentPassword, newPassword, confirmPassword) {
            var args = {
                id: userId,
                current_password: currentPassword,
                new_password: newPassword,
                confirm_password: confirmPassword
            };
            return dataService.update(endpoint, args);
        }
    }
}());
