/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('applicationsData', AppsDataFactory);

    AppsDataFactory.$inject = ['dataServiceSimple'];

    function AppsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'applications';

        dataFactory.applications = applications;
        dataFactory.application = {
            get: getApplication,
            updateStatus: updateStatus
        };

        return dataFactory;

        function getApplication(deviceId, applicationId) {
            var args = {
                device_id: deviceId,
                id: applicationId
            };
            return dataService.get(endpoint, args);
        }

        function applications(offset, limit, deviceId, applicationId) {
            var args = {offset: offset, limit: limit, device_id: deviceId};
            if (angular.isDefined(applicationId)) {
                args.push({id: applicationId});
            }
            return dataService.get(endpoint, args);
        }

        function updateStatus(id, alertStatusId, restrictionStatusId) {
            var args = {
                id: id,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.update(endpoint, args);
        }
    }
}());
