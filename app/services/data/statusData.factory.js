/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('statusData', CallsDataFactory);

    CallsDataFactory.$inject = ['dataServiceSimple'];

    function CallsDataFactory(dataService) {
        var dataFactory = {};

        dataFactory.alertStatuses = alertStatuses;
        dataFactory.restrictionStatus = restrictionStatuses;

        return dataFactory;

        function alertStatuses() {
            return dataService.get('alert_statuses');
        }
        function restrictionStatuses() {
            return dataService.get('restriction_statuses');
        }
    }
}());
