/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('messagesData', DataFactory);

    DataFactory.$inject = ['dataServiceSimple'];

    function DataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'messages';

        dataFactory.messages = messages;
        dataFactory.getByContact = getByContact;

        return dataFactory;

        function messages(offset, limit, deviceId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId
            };
            return dataService.get(endpoint, args);
        }

        function getByContact(offset, limit, deviceId, logContactId) {
            var args = {
                offset: offset,
                limit: limit,
                device_id: deviceId,
                log_contact_id: logContactId
            };
            return dataService.get(endpoint, args);
        }
    }
}());
