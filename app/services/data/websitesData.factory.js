/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('websitesData', SiteDataFactory);

    SiteDataFactory.$inject = ['dataServiceSimple'];

    function SiteDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'websites';

        dataFactory.websites = websites;
        dataFactory.website = {
            create: websiteCreate,
            read: websiteRead,
            update: websiteUpdate,
            updateStatus: websiteUpdateStatus,
            remove: websiteRemove
        };

        return dataFactory;

        function websites(offset, limit, deviceId) {
            var args = {offset: offset, limit: limit, device_id: deviceId};
            return dataService.get(endpoint, args);
        }

        function websiteRead(deviceId, webSiteId) {
            return dataService.get(endpoint, {device_id: deviceId, id: webSiteId});
        }

        function websiteUpdate(websiteId, name, domain, alertStatusId, restrictionStatusId) {
            var args = {
                id: websiteId,
                name: name,
                domain: domain,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.update(endpoint, args);
        }

        function websiteUpdateStatus(websiteId, alertStatusId, restrictionStatusId) {
            var args = {
                id: websiteId,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.update(endpoint, args);
        }

        function websiteCreate(name, domain, alertStatusId, restrictionStatusId) {
            var args = {
                name: name,
                domain: domain,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            };
            return dataService.add(endpoint, args);
        }

        function websiteRemove(webSiteId) {
            return dataService.delete(endpoint, {id: webSiteId});
        }
    }
}());
