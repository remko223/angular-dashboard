/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('accountsData', AccountsDataFactory);

    AccountsDataFactory.$inject = ['dataServiceSimple'];

    function AccountsDataFactory(dataService) {
        var dataFactory = {},
            endpoint = 'accounts';

        dataFactory.accounts = accounts;
        dataFactory.account = {
            create: accountCreate,
            update: accountUpdate,
            remove: accountRemove,
            forgotPassword: forgotPassword,
            alerts: setAlerts,
            dailyWatch: setDailyWatch,
            defaultDeviceId: setDefaultDeviceId
        };

        return dataFactory;

        function accounts() {
            return dataService.get(endpoint);
        }

        function accountCreate(firstName, lastName, email, password, confirmPassword) {
            var args = {
                first_name: firstName,
                last_name: lastName,
                email: email,
                password: password,
                confirm_password: confirmPassword
            };
            return dataService.add(endpoint, args);
        }

        function setAlerts(enabled) {
            return dataService.update(endpoint, {is_alerts_enabled: enabled});
        }

        function setDailyWatch(enabled) {
            return dataService.update(endpoint, {is_daily_watch_enabled: enabled});
        }

        function setDefaultDeviceId(deviceId) {
            return dataService.update(endpoint, {default_device_id: deviceId});
        }

        function accountUpdate(accountStatusId, alertsEnabled, dailyWatchEnabled, defaultDeviceId, note, active) {
            var args = {
                account_status_id: accountStatusId,
                is_alerts_enabled: alertsEnabled,
                is_daily_watch_enabled: dailyWatchEnabled,
                default_device_id: defaultDeviceId,
                note: note,
                is_active: active
            };
            return dataService.update(endpoint, args);
        }

        function accountRemove(reason) {
            return dataService.delete(endpoint, {reason: reason});
        }

        function forgotPassword(email) {
            return dataService('forgot', {email: email});
        }
    }
}());
