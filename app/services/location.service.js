/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .service('locationService', LocationService);

    LocationService.$inject = ['dataService', '$filter', 'growl', '_', '$q', '$interval'];

    function LocationService(dataService, $filter, growl, _, $q, $interval) {
        return {
            lastLocation: lastLocation,
            loadAll: loadAll,
            createMarker: createMarker
        };

        function lastLocation(deviceId) {
            var deferred = $q.pending(),
                last,
                interval;

            dataService
                .locations(deviceId, 0, 1)
                .then(function (response) {
                    var locations = response.locations;

                    if (locations && locations.length) {
                        last = locations[0];
                    }

                    return dataService.location.last(deviceId);
                })
                .then(function () {
                    interval = $interval(fetchLocation, 500, 20);

                    return interval;
                })
                .then(function (times) {
                    var marker = last ? createMarker(last, true) : undefined;

                    // This is called when interval runs <count> times meaning it won't run anymore
                    // when cancel is called on interval it throws an error instead
                    deferred.resolve([marker]);

                    if (last) {
                        growl.warning('Unable to fetch the current location, showing last known location', {ttl: 5000});
                    } else {
                        growl.error('Unable to fetch current location.', {ttl: 10000});
                    }
                });

            function fetchLocation() {
                dataService
                    .locations(deviceId, 0, 1)
                    .then(function (response) {
                        var locations = response.locations, marker;

                        if (locations && locations.length) {
                            if (!angular.equals(locations[0].log.device_created_on, last.log.device_created_on)) {
                                $interval.cancel(interval);

                                marker = createMarker(locations[0], true);

                                deferred.resolve([marker]);
                            }
                        }
                    });
            }

            return deferred.promise;
        }

        function loadAll(deviceId) {
            return dataService
                .locations(deviceId, 0, 10)
                .then(function (response) {
                    var first = true, markers = [];

                    angular.forEach(response.locations, function (location) {
                        var marker = createMarker(location, first);

                        if (first) {
                            first = false;
                        }

                        markers.push(marker);
                    });

                    return markers;
                });
        }

        function createMarker(location, first) {
            var wdDate = $filter('wdDate'),
                result = {
                    lat: Number(location.latitude),
                    lng: Number(location.longitude),
                    icon: {},
                    time: wdDate(location.log.device_created_on)
                },
                place;

            if (location.place) {
                place = _.mapValues(location.place, function (value) {
                    return value ? value : '';
                });

                if (place.display_name === '') {
                    result.message = (place.house_number + ' ' + place.road).trim();

                    if (result.message !== '') {
                        result.message += '<br>';
                    }

                    result.message += place.city;

                    if (place.city !== '') {
                        result.message += ', ';
                    }

                    result.message += place.state + ' ' + place.postcode;

                    result.message = result.message.trim();

                    if (result.message !== '' && !result.message.match(/<br>$/)) {
                        result.message += '<br>';
                    }
                } else {
                    result.message = place.display_name + '<br>';
                }
            }

            result.message += '<small class="text-muted"><span class="glyphicon glyphicon-time"></span> ' +
                result.time + '</small>';

            if (first) {
                result.icon = {
                    type: 'awesomeMarker',
                    icon: 'star',
                    markerColor: 'red'
                };
                result.focus = true;
            }

            return result;
        }
    }
}());
