/* jshint camelcase:false */
/* jshint maxstatements:80 */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .factory('dataService', DataService);

    DataService.$inject = ['appConfig', '$resource', 'actions', '$window'];

    function DataService(appConfig, $resource, actions, $window) {
        var dataService = {},
            resources = {};

        // Account methods
        dataService.accounts = accounts;
        dataService.account = {
            create: accountCreate,
            update: accountUpdate,
            remove: accountRemove,
            forgotPassword: forgotPassword,
            validate: accountValidate,
            reset: passwordReset
        };

        dataService.devices = devices;
        dataService.device = {
            add: deviceAdd,
            read: deviceRead,
            remove: deviceRemove
        };

        dataService.sendSmsLink = sendSmsLink;
        dataService.sync = sync;

        dataService.users = users;
        dataService.user = {
            read: userRead,
            update: userUpdate
        };

        dataService.dashboard = dashboard;
        dataService.calls = calls;
        dataService.contacts = contacts;
        dataService.contact = {
            updateStatus: contactUpdateStatus,
            remove: contactRemove,
            create: contactCreate,
            update: contactUpdate
        };

        dataService.timeblocks = timeblocks;
        dataService.timeblock = {
            updateStatus: timeBlockUpdateStatus,
            remove: timeBlockRemove,
            create: timeBlockCreate,
            update: timeBlockUpdate
        };
        dataService.emergencyContacts = emergencyContacts;
        dataService.emergencyContact = {
            remove: emergencyContactRemove,
            add: emergencyContactCreate,
            update: emergencyContactUpdate
        };

        dataService.messages = messages;

        // Website methods
        dataService.websites = websites;

        dataService.website = {
            create: websiteCreate,
            read: websiteRead,
            update: websiteUpdate,
            updateStatus: websiteUpdateStatus,
            remove: websiteRemove
        };

        // Location methods
        dataService.locations = locations;
        dataService.location = {
            last: locationLast
        };

        dataService.applications = applications;
        dataService.application = {
            updateStatus: applicationUpdateStatus
        };

        dataService.status = {
            alert: statusAlert,
            restriction: statusRestriction
        };

        dataService.icloud = icloud;
        dataService.reports = reports;
        dataService.report = {
            updateReportItem: updateReportItem
        };

        activate();

        return dataService;

        function activate() {
            resourceAccounts();

            resources.sendSmsLink = $resource(appConfig.apiUrl + '/device_resend_registration', undefined, actions);
            resources.sync = $resource(appConfig.apiUrl + '/sync_device', undefined, actions);
            resources.users = $resource(appConfig.apiUrl + '/users', undefined, actions);
            resources.forgot = $resource(appConfig.apiUrl + '/forgot');
            resources.devices = $resource(appConfig.apiUrl + '/devices', undefined, actions);
            resources.icloud = $resource(appConfig.apiUrl + '/icloud', undefined, actions);
            resources.dashboard = $resource(appConfig.apiUrl + '/dashboard', undefined, actions);
            resources.calls = $resource(appConfig.apiUrl + '/calls', undefined, actions);
            resources.contacts = $resource(appConfig.apiUrl + '/contacts', undefined, actions);
            resources.timeblocks = $resource(appConfig.apiUrl + '/time_block', undefined, actions);
            resources.messages = $resource(appConfig.apiUrl + '/messages', undefined, actions);
            resources.websites = $resource(appConfig.apiUrl + '/websites', undefined, actions);
            resources.applications = $resource(appConfig.apiUrl + '/applications', undefined, actions);
            resources.statusAlert = $resource(appConfig.apiUrl + '/alert_statuses', undefined, actions);
            resources.statusRestriction = $resource(appConfig.apiUrl + '/restriction_statuses', undefined, actions);
            resources.locations = $resource(appConfig.apiUrl + '/locations', undefined, actions);
            resources.alertAddresses = $resource(appConfig.apiUrl + '/alert_addresses', undefined, actions);
            resources.billings = $resource(appConfig.apiUrl + '/billings', undefined, actions);
            resources.reports = $resource(appConfig.apiUrl + '/logs', undefined, actions);
            resources.validate = $resource(appConfig.apiUrl + '/email_validation/:guid', undefined, actions);
            resources.reset = $resource(appConfig.apiUrl + '/reset_password', undefined, actions);
            resources.emergencyContacts = $resource(appConfig.apiUrl + '/emergency_addresses', undefined, actions);
        }

        function resourceAccounts() {
            var accountActions = angular.copy(actions);

            accountActions.get.transformResponse = function (data, headersGetter) {
                var response = angular.fromJson(data);

                // This way we can just update the object and call update
                return response.content;
            };

            accountActions.update.transformRequest = function (data, headersGetter) {
                var fields = ['product_id', 'is_active', 'promo_id', 'created_on', 'modified_on', 'id'];

                angular.forEach(fields, function (field) {
                    data[field] = undefined;
                });

                return angular.toJson(data);
            };

            resources.accounts = $resource(appConfig.apiUrl + '/accounts', undefined, accountActions);
        }

        function accounts() {
            return resources.accounts.get().$promise;
        }

        function devices() {
            return resources.devices.get().$promise;
        }

        function deviceAdd(android, deviceData) {
            if (android) {
                return resources
                    .devices
                    .add({
                        phone_number: deviceData.phone_number,
                        display_name: deviceData.display_name,
                        device_platform_id: 1
                    }).$promise;
            } else {
                return resources
                    .devices
                    .add({
                        phone_number: deviceData.phone_number,
                        display_name: deviceData.display_name,
                        device_platform_id: 2,
                        uuid: deviceData.uuid,
                        friendly_name: deviceData.friendly_name,
                        icu: deviceData.icu,
                        icp: deviceData.icp,
                        device_key: deviceData.device_key,
                        ios_version: deviceData.ios_version,
                        device_name: deviceData.device_name,
                        device_model: deviceData.device_model,
                        device_serial: deviceData.device_serial,
                        phone_name: deviceData.phone_name,
                        latest_backup: deviceData.latest_backup
                    }).$promise;
            }
        }

        function sendSmsLink(deviceId) {
            return resources
                .sendSmsLink
                .get({type: 'sms', device_id: deviceId})
                .$promise;
        }

        function sync(deviceId) {
            return resources
                .sync
                .get({device_id: deviceId})
                .$promise;
        }

        function deviceRemove(deviceId) {
            return resources.devices.remove({id: deviceId}).$promise;
        }

        function users() {
            return resources.users.get({offset: 0, limit: 1}).$promise;
        }

        function userRead() {
            var users = resources.users.get({offset: 0, limit: 1});

            if (users && (users.length >= 1)) {
                return users[0].$promise;
            }
        }

        function userUpdate(user) {
            return resources
                .users
                .update(user, {})
                .$promise;
        }

        function dashboard() {
            return resources.dashboard.get().$promise;
        }

        function calls(offset, limit, deviceId, contactId) {
            return resources.calls.get({
                offset: offset,
                limit: limit,
                device_id: deviceId,
                contact_id: contactId
            }).$promise;
        }

        function contacts(offset, limit, deviceId, contactId) {
            return resources.contacts.get({offset: offset, limit: limit, device_id: deviceId, id: contactId}).$promise;
        }

        function contactRemove(contactId) {
            return resources.contacts.remove({id: contactId}).$promise;
        }

        function contactCreate(deviceId, firstName, lastName, contactJson) {
            return resources.contacts.add({
                device_id: deviceId,
                first_name: firstName,
                last_name: lastName,
                contact_json: angular.toJson(contactJson)
            }).$promise;
        }

        function contactUpdate(contactId, firstName, lastName, contactJson) {
            return resources.contacts.update({
                id: contactId,
                first_name: firstName,
                last_name: lastName,
                contact_json: angular.toJson(contactJson)
            }).$promise;
        }

        function contactUpdateStatus(contactId, alertStatusId, restrictionStatusId) {
            return resources
                .contacts
                .update({
                    id: contactId,
                    alert_status_id: alertStatusId,
                    restriction_status_id: restrictionStatusId
                })
                .$promise;
        }

        // EMERGENCY CONTACTS
        function emergencyContacts() {
            return resources.emergencyContacts.get().$promise;
        }

        function emergencyContactRemove(id) {
            return resources.emergencyContacts.remove({id: id}).$promise;
        }

        function emergencyContactCreate(econtact) {
            return resources.emergencyContacts.add({
                phone_number: econtact.phone_number,
                description: econtact.description
            }).$promise;
        }

        function emergencyContactUpdate(econtact) {
            return resources.emergencyContacts.update({
                id: econtact.id,
                phone_number: econtact.phone_number,
                description: econtact.description
            }).$promise;
        }

        function timeblocks(offset, limit, deviceId, timeBlockId) {
            return resources.timeblocks.get({offset: offset, limit: limit, device_id: deviceId, id: timeBlockId}).$promise;
        }

        function timeBlockRemove(deviceId, timeBlockId) {
            return resources.timeblocks.remove({device_id: deviceId, time_block_id: timeBlockId}).$promise;
        }

        function timeBlockCreate(deviceId, displayName, timeBlockJson) {
            return resources.timeblocks.add({
                device_id: deviceId,
                display_name: timeBlockJson.timeBlockName,
                start_on: parseInt(timeBlockJson.startTime.id),
                end_on: parseInt(timeBlockJson.endTime.id),
                sun: timeBlockJson.days.sun,
                mon: timeBlockJson.days.mon,
                tue: timeBlockJson.days.tue,
                wed: timeBlockJson.days.wed,
                thu: timeBlockJson.days.thu,
                fri: timeBlockJson.days.fri,
                sat: timeBlockJson.days.sat,
                is_enabled: timeBlockJson.isEnabled
            }).$promise;
        }

        function timeBlockUpdate(deviceId, timeblockId, timeBlockJson) {
            return resources.timeblocks.update({
                time_block_id: timeblockId,
                device_id: deviceId,
                display_name: timeBlockJson.timeBlockName,
                start_on: parseInt(timeBlockJson.startTime.id),
                end_on: parseInt(timeBlockJson.endTime.id),
                sun: timeBlockJson.days.sun,
                mon: timeBlockJson.days.mon,
                tue: timeBlockJson.days.tue,
                wed: timeBlockJson.days.wed,
                thu: timeBlockJson.days.thu,
                fri: timeBlockJson.days.fri,
                sat: timeBlockJson.days.sat,
                is_enabled: timeBlockJson.isEnabled
            }).$promise;
        }

        function timeBlockUpdateStatus(deviceId, timeBlockId, isEnabled) {
            $window.console.log('timeBlockUpdateStatus', deviceId + '/' + timeBlockId + '/' + isEnabled);
            return resources
                .timeblocks
                .update({
                    time_block_id: timeBlockId,
                    device_id: deviceId,
                    is_enabled: isEnabled
                })
                .$promise;
        }

        function messages(offset, limit, deviceId, contactId) {
            return resources.messages.get({
                offset: offset,
                limit: limit,
                device_id: deviceId,
                log_contact_id: contactId
            }).$promise;
        }

        function websites(offset, limit, deviceId, siteId) {
            return resources.websites.get({offset: offset, limit: limit, device_id: deviceId, id: siteId}).$promise;
        }

        function websiteRead(webSiteId) {
            return resources.websites.get({offset: 0, limit: 1, id: webSiteId}).$promise;
        }

        function deviceRead(deviceId) {
            return resources.devices.get({id: deviceId}).$promise;
        }

        function websiteUpdate(website) {
            return resources
                .websites
                .update({
                    id: website.id,
                    name: website.name,
                    domain: website.domain,
                    alert_status_id: website.alert_status_id,
                    restriction_status_id: website.restriction_status_id,
                    is_active: website.is_active
                })
                .$promise;
        }

        function websiteUpdateStatus(websiteId, alertStatusId, restrictionStatusId) {
            return resources
                .websites
                .update({
                    id: websiteId,
                    alert_status_id: alertStatusId,
                    restriction_status_id: restrictionStatusId
                })
                .$promise;
        }

        function websiteCreate(website) {
            return resources
                .websites
                .add({
                    device_id: website.device_id,
                    name: website.name,
                    domain: website.domain,
                    alert_status_id: website.alert_status_id,
                    restriction_status_id: website.restriction_status_id
                })
                .$promise;
        }

        function websiteRemove(webSiteId) {
            return resources.websites.remove({id: webSiteId}).$promise;
        }

        function applications(offset, limit, deviceId) {
            return resources.applications.get({offset: offset, limit: limit, device_id: deviceId}).$promise;
        }

        function applicationUpdateStatus(id, alertStatusId, restrictionStatusId) {
            return resources.applications.update({
                id: id,
                alert_status_id: alertStatusId,
                restriction_status_id: restrictionStatusId
            }).$promise;
        }

        function statusAlert() {
            return resources.statusAlert.get().$promise;
        }

        function statusRestriction() {
            return resources.statusRestriction.get().$promise;
        }

        function locations(deviceId, offset, limit) {
            return resources.locations.get({device_id: deviceId, offset: offset, limit: limit}).$promise;
        }

        function locationLast(deviceId) {
            return resources.locations.update({device_id: deviceId}).$promise
                .then(function () {
                    return resources.locations.get({device_id: deviceId, offset: 0, limit: 1});
                });
        }

        /**
         * Creates a new account, used by the sign up form.
         *
         * @param firstName {string}
         * @param lastName {string}
         * @param email {string}
         * @param password {string}
         * @param confirmPassword {string}
         * @returns {promise}
         */
        function accountCreate(firstName, lastName, email, password, confirmPassword, promo) {
            return resources
                .accounts
                .add({
                    first_name: firstName,
                    last_name: lastName,
                    email: email,
                    password: password,
                    confirm_password: confirmPassword,
                    promo_code: promo
                }).$promise;
        }

        /**
         * Updates the currently logged in account.
         *
         * @param accountStatusId {number}
         * @param alertsEnabled {boolean}
         * @param dailyWatchEnabled {boolean}
         * @param defaultDeviceId {number}
         * @param note {string}
         * @param active {boolean}
         * @returns {promise}
         */
        function accountUpdate(accountStatusId, alertsEnabled, dailyWatchEnabled, defaultDeviceId, note, active) {
            return resources
                .accounts
                .update({
                    account_status_id: accountStatusId,
                    is_alerts_enabled: alertsEnabled,
                    is_daily_watch_enabled: dailyWatchEnabled,
                    default_device_id: defaultDeviceId,
                    note: note,
                    is_active: active
                })
                .$promise;
        }

        function accountRemove(accountId) {
            return resources
                .accounts
                .remove({id: accountId});
        }

        function forgotPassword(email) {
            return resources
                .forgot
                .save({email: email})
                .$promise;
        }

        function accountValidate(guid) {
            return resources
                .validate
                .get({guid: guid})
                .$promise;
        }

        function passwordReset(guid) {
            return resources
                .reset
                .get({uuid: guid})
                .$promise;
        }

        function reports(criteria) {
            return resources.reports.get(criteria).$promise;
        }

        function updateReportItem(criteria) {
            return resources.reports.update(criteria).$promise;
        }

        function icloud(username, password) {
            return resources.icloud.save({login: username, password: password}).$promise;
        }
    }
}());
