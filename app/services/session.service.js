(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .service('session', SessionService);

    SessionService.$inject = ['$window', '$cookies'];

    function SessionService($window, $cookies) {
        var session = {};

        Object.defineProperty(session, 'deviceId', {
            get: function () {
                return $cookies.getObject('deviceId');
            },
            set: function (device) {
                if (device) {
                    $cookies.putObject('deviceId', device);
                } else {
                    $cookies.remove('deviceId');
                }
            }
        });

        Object.defineProperty(session, 'loggedIn', {
            get: function () {
                return $cookies.getObject('loggedIn');
            },
            set: function (loggedIn) {
                if (loggedIn) {
                    $cookies.putObject('loggedIn', loggedIn);
                } else {
                    $cookies.remove('loggedIn');
                }
            }
        });

        Object.defineProperty(session, 'isActiveSubscriber', {
            get: function () {
                var tmp = $cookies.getObject('isActiveSubscriber');

                if (tmp) {
                    return tmp;
                } else {
                    return false;
                }
            },
            set: function (active) {
                if (active) {
                    $cookies.putObject('isActiveSubscriber', active);
                } else {
                    $cookies.remove('isActiveSubscriber');
                }
            }
        });

        return session;
    }
}());
