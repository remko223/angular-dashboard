/* jshint camelcase:false */
(function () {
    'use strict';

    angular
        .module('WatchDogApp')
        .controller('SecureController', SecureController);

    SecureController.$inject = ['$scope', 'session', 'dataService', 'userService', '$timeout', '$window'];

    /* @ngInject */
    function SecureController($scope, session, dataService, userService, $timeout, $window) {
        var vm = this;

        vm.selectDevice = selectDevice;
        vm.selectedClass = selectedClass;
        vm.user = userService;

        activate();

        function activate() {
            if (!session.loggedIn) {
                $scope.$state.go('login');
                return;
            }

            $scope.$on('deviceChange', loadDevices);
            // $scope.$on('userChange', loadDevices);

            // loadDevices();
        }

        /**
         * Need it as a function so that it can be called by the promise if necessary.
         */
        function prepareDevice() {
            var deviceId = session.deviceId,
                exists = false;

            if (deviceId) {
                // Check that selected deviceId still exists
                angular.forEach(vm.devices, function (device) {
                    if (device.id === deviceId) {
                        exists = true;
                    }
                });

                if (!exists) {
                    session.deviceId = undefined;
                }
            }

            // Only set deviceId if it's not already set
            if (!session.deviceId && vm.devices.length > 0) {
                session.deviceId = vm.devices[0].id;
            }

            deviceId = session.deviceId;
            userService.selectedDeviceId = deviceId;

            angular.forEach(vm.devices, function (device) {
                if (device.id === deviceId) {
                    selectDevice(device);
                }
            });
        }

        function loadDevices() {
            return dataService
                .dashboard()
                .then(function (response) {
                    vm.devices = response.devices;

                    if (!vm.devices || vm.devices.length === 0) {
                        // $window.alert('now redirecting to chooser!');
                        $scope.$state.go('secure.wizard.choose');
                    }

                    return response.devices;
                })
                .then(prepareDevice);
        }

        function selectDevice(device) {
            session.deviceId = device.id;

            vm.selectedDevice = device;

            dataService
                .accounts()
                .then(function (account) {
                    if (account.default_device_id !== device.id) {
                        account.default_device_id = device.id;

                        account.$update();
                    }
                });

            $timeout(function () {
                $scope.$broadcast('selectDevice', device);
            }, 100);
        }

        function selectedClass(deviceId) {
            return $scope.main.user.selectedDeviceId !== deviceId ? [] : ['selected'];
        }
    }
})();
